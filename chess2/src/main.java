import java.util.Arrays;
import java.util.Scanner;
import java.io.*;

public class main {
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        boolean skip = true;
        int [][] redpawn = new int[8][2];
        int [][] blackpawn = new int[8][2];
        int [] rindex = {2,2,2};
        int [] bindex = {2,2,2};
        int [][] baricade = new int[8][2];
        int [][] wall = new int[8][2];
        int [][] targetlist = new int [8][2];
        int [][] health = {{15,15,10,10,12,12},{15,15,10,10,12,12}};
        int [] empty = {0,0};
        int [][] heathpotion = {{1,1,1,1,1,1},{1,1,1,1,1,1}};
        boolean turn = false;
        int x = (int) ((Math.random() * (3 - 1)) + 1);
        int y = (int) ((Math.random() * (2 - 1)) + 1);
        int turncount = 0;
        //walls&baricades
        for(int i = 0;i <= x;i = i + 2) {
            baricade[i][0] = (int) ((Math.random() * (5 - 3)) + 3);
            baricade[i][1] = (int) ((Math.random() * (9 - 1)) + 1);
            baricade[i+1][0] = baricade[i][0];
            baricade[i+1][1] = baricade[i][1] + 1;
        }
        for(int i = 0;i <= y;i++) {
            wall[y][0] = (int) ((Math.random()*(5-3+1)+3));
            wall[y][1] = (int) ((Math.random()*(9-1+1)+1));
            wall[y+1][0] = wall[y][0];
            wall[y+1][1] = wall[y][1] + 1;
        }
        render(redpawn,blackpawn,baricade,wall,targetlist);
//main loop
        //pawnplace
while(true) {
    for (int i = 1; i <= 12 && skip; i++) {
        System.out.println(i);
        if (turn) {
            System.out.println("\u001B[31m" + "Червените са на ход");
            redpawn = spawnpawn(redpawn, turn, rindex, bindex,redpawn,blackpawn,baricade,wall);
            turn = false;

        } else {
            System.out.println("\u001B[30m" + "Черните са на ход");
            blackpawn = spawnpawn(blackpawn, turn, rindex, bindex,redpawn,blackpawn,baricade,wall);
            turn = true;
        }
        System.out.println(Arrays.deepToString(redpawn));
        System.out.println(Arrays.deepToString(blackpawn));
        render(redpawn,blackpawn,baricade,wall,targetlist);
        String temp = obj.nextLine();
        if(temp.equals("y")){
            skip = false;
        }
    }
    //begin game
    boolean turnend = false;
    int deathcount = 0;
    int[] pawn = new int[2];
    if(turn){
        for(int i = 1;i <=6;i++){
            if(Arrays.equals(redpawn[i],empty)){
                deathcount++;
            }
        }
        if(deathcount == 6){break;} else {
            while (!turnend) {
                targetlist = possbileattacks(turn, redpawn, blackpawn, 0);
                render(redpawn, blackpawn, baricade, wall, targetlist);
                System.out.println("\u001B[31m" + "Червените са на ход : Изберете деиствие");
                System.out.println("1- придвижване\n" +
                        "2-аттакакуване\n" +
                        "3-лекуване");
                switch (obj.nextInt()) {
                    case 1:
                        redpawn = movepawn(turn, redpawn, blackpawn, baricade, wall);
                        turnend = true;
                        break;
                    case 2:
                        pawn = pawnselect(turn, redpawn, blackpawn);
                        pawnattack(turn, redpawn, blackpawn, pawn[0], targetlist, health, baricade);
                        for (int i = 0; i <= 5; i++) {
                            if (health[0][i] <= 0) {
                                blackpawn[i] = empty;
                            }
                        }
                        turnend = true;
                        break;
                    case 3:
                        int maxhp = 15;
                        pawn = pawnselect(turn, redpawn, blackpawn);
                        switch (pawn[1]) {
                            case 1:
                                maxhp = 15;
                            case 2:
                                maxhp = 10;
                            case 3:
                                maxhp = 12;
                        }
                        if (health[0][pawn[0]] < maxhp && heathpotion[0][pawn[0]] == 1) {
                            pawnheal(turn, health, heathpotion, pawn[0], pawn[1]);
                            heathpotion[0][pawn[0]] = 0;
                            turnend = true;
                        } else {
                            System.out.println("Не можете да лекувате този герои");
                            turnend = false;
                        }
                        if((int) ((Math.random() * (6 - 1))) + 1 == 6){
                            System.out.println("героят изсипва отварата колкото се може по бързо : получавате още един ход");
                            turnend = false;
                        }
                        break;


                }
            }
        }
        turn = false;
        turncount++;
    } else {
        for(int i = 1;i <=6;i++){
            if(Arrays.equals(blackpawn[i],empty)){
                deathcount++;
            }
        }
        if(deathcount == 6){break;} else {

            while (!turnend) {
                targetlist = possbileattacks(turn, redpawn, blackpawn, 0);
                render(redpawn, blackpawn, baricade, wall, targetlist);
                System.out.println("\u001B[30m" + "Черните са на ход : Изберете деиствие");
                System.out.println("1- придвижване\n" +
                        "2-аттакакуване\n" +
                        "3-лекуване");
                switch (obj.nextInt()) {
                    case 1:
                        blackpawn = movepawn(turn, redpawn, blackpawn, baricade, wall);
                        turnend = true;
                        break;
                    case 2:
                        pawn = pawnselect(turn, redpawn, blackpawn);
                        pawnattack(turn, redpawn, blackpawn, pawn[0], targetlist, health, baricade);
                        for (int i = 0; i <= 5; i++) {
                            if (health[0][i] <= 0) {
                                redpawn[i] = empty;
                            }
                        }
                        turnend = true;
                        break;
                    case 3:
                        int maxhp = 15;
                        pawn = pawnselect(turn, redpawn, blackpawn);
                        switch (pawn[1]) {
                            case 1:
                                maxhp = 15;
                            case 2:
                                maxhp = 10;
                            case 3:
                                maxhp = 12;
                        }
                        if (health[1][pawn[0]] < maxhp && heathpotion[1][pawn[0]] == 1) {
                            pawnheal(turn, health, heathpotion, pawn[0], pawn[1]);
                            heathpotion[1][pawn[0]] = 0;
                            turnend = true;
                        } else {
                            System.out.println("Не можете да лекувате този герои");
                            turnend = false;
                        }
                        if((int) ((Math.random() * (6 - 1))) + 1 == 6){
                            System.out.println("героят изсипва отварата колкото се може по бързо : получавате още един ход");
                            turnend = false;
                        }
                        break;


                }
            }
        }
        turn = true;
        turncount++;
    }
    }
int score = 0;

        System.out.println(turncount + " хода бяха изиграни : червените завършиха със " + score + " точки." );
        System.out.println("Повалените червени герои бяха");
        for(int i = 0;i <= 6;i++){

            if(Arrays.equals(redpawn[i],empty) ){
                switch(i){
                    case 1,2:
                        System.out.println(i + "-Рицар-%");
                        break;
                    case 3,4:
                        System.out.println(i +"-Джудже-@");
                        break;
                    case 5,6:
                        System.out.println(i +"-Елф-&");
                        break;
                }
            }
        }
        System.out.println("Повалените черни герои бяха");
        for(int i = 0;i <= 6;i++){
            score++;
            if(Arrays.equals(blackpawn[i],empty) ){
                switch(i){
                    case 1,2:
                        System.out.println(i + "-Рицар-%");
                        break;
                    case 3,4:
                        System.out.println(i +"-Джудже-@");
                        break;
                    case 5,6:
                        System.out.println(i +"-Елф-&");
                        break;
                }
            }
        }


    }


    public static void render(int[][] redpawn,int[][] blackpawn,int[][] baricade,int[][] wall,int[][]targetlist){


        for (int row = 1; row <= 7; row++) {     //v^
            for (int col = 1; col <= 9; col++) { //<>
                String color = "\u001B[0m";
                String symbol = "*";
                if(row <= 2){color = "\u001B[31m";}
                if(row >= 6){color = "\u001B[30m";}
                for(int i = 0;i<=4;i++){
                    if(baricade[i][0] == row && baricade[i][1] == col) {
                        symbol = "$";
                    }
                    if(wall[i][0] == row && wall[i][1] == col) {
                        symbol = "#";
                    }
                }



                //pawns
                for(int i = 0;i <= 6;i++){
                        if (redpawn[i][0] == row && redpawn[i][1] == col)
                        {       color = "\u001B[31m";
                            switch(i){
                            case 1,2:
                                symbol = "%";
                                break;
                            case 3,4:
                                symbol = "@";
                                break;
                            case 5,6:
                                symbol = "&";
                                break;
                        }}
                    if (blackpawn[i][0] == row && blackpawn[i][1] == col)
                    {   color = "\u001B[30m";
                        switch(i){
                        case 1,2:
                            symbol = "%";
                            break;
                        case 3,4:
                            symbol = "@";
                            break;
                        case 5,6:
                            symbol = "&";
                            break;
                        }
                    }
                    if(targetlist[i][0] == row && targetlist[i][1] == col)
                    {   color = "\u001B[33m";
                        switch(i){
                        case 1,2:
                            symbol = "%";
                            break;
                        case 3,4:
                            symbol = "&";
                            break;
                        case 5,6:
                            symbol = "@";
                            break;
                    }

                    }
                }

                System.out.print(color + " " + symbol + " ");
            }
            System.out.println("\u001B[0m" + " ");
        }

    }

    public static int[][] spawnpawn(int[][] pawn ,boolean redturn , int[] reddindex, int[] bindex, int[][] redpawn,int[][] blackpawn, int[][] barricade,int[][] wall){
        Scanner input = new Scanner(System.in);
        variable game = new variable();
        int[] redindex = reddindex;
        int[] blackindex = bindex;
        int[] deadinex = {0,0,0};
        int pawnindex = 0;
        boolean end = false;
        while(!end) {
            if (redturn) {
                System.out.println("Разполагате със следните фигури");
                System.out.println("(" + redindex[0] + ")" + "рицар - 1" + "\n" +
                        "(" + redindex[1] + ")" + "Джудже - 2" + "\n" +
                        "(" + redindex[2] + ")" + "Елф - 3" + "\n");

                System.out.println("Коя единица искате да позиционирате?");
                int pinput = input.nextInt();
                if(pinput < 4) {
                    if (redindex[pinput - 1] > 0) {
                        System.out.println("На кои ред");
                        int row_input = input.nextInt();
                        System.out.println("На коия колона");
                        int col_input = input.nextInt();
                        int[] coord = {col_input,row_input};
                        if (Arrays.equals(reddindex,deadinex) && Arrays.equals(blackindex,deadinex)){end=true;}
                        if (row_input > 0 && row_input < 3 && col_input > 0 && col_input < 10 && collisioncheck(coord,redpawn,blackpawn,barricade,wall,0)) {
                            switch (pinput) {
                                case 1:
                                    if (redindex[pinput - 1] == 2) {
                                        pawnindex = 1;
                                    }
                                    if (redindex[pinput - 1] == 1) {
                                        pawnindex = 2;
                                    }
                                    break;
                                case 2:
                                    if (redindex[pinput - 1] == 2) {
                                        pawnindex = 3;
                                    }
                                    if (redindex[pinput - 1] == 1) {
                                        pawnindex = 4;
                                    }
                                    break;
                                case 3:
                                    if (redindex[pinput - 1] == 2) {
                                        pawnindex = 5;
                                    }
                                    if (redindex[pinput - 1] == 1) {
                                        pawnindex = 6;
                                    }
                                    break;
                            }
                            redindex[pinput - 1] = redindex[pinput - 1] - 1;
                            game.setRedindex(redindex);
                            pawn[pawnindex][0] = row_input;
                            pawn[pawnindex][1] = col_input;
                            end = true;
                        } else {System.out.println("Невалидна позиция");}
                    } else {
                        System.out.println("Нямате повече членове от този клас");
                    }
                } else {System.out.println("Нямаме такъв воиник");}

                //BLACK

            } else {
                System.out.println("Разполагате със следните фигури");
                System.out.println("(" + blackindex[0] + ")" + "рицар - 1" + "\n" +
                        "(" + blackindex[1] + ")" + "Джудже - 2" + "\n" +
                        "(" + blackindex[2] + ")" + "Елф - 3" + "\n");

                System.out.println("Коя единица искате да позиционирате?");
                int pinput = input.nextInt();
                if(pinput < 4) {
                    if (blackindex[pinput - 1] > 0) {
                        System.out.println("На кои ред");
                        int row_input = input.nextInt();
                        System.out.println("На коия колона");
                        int col_input = input.nextInt();
                        int[] coord = {row_input,col_input};
                        if (row_input > 5 && row_input <= 7 && col_input > 0 && col_input < 10 && collisioncheck(coord,redpawn,blackpawn,barricade,wall,0)) {
                            switch (pinput) {
                                case 1:
                                    if (blackindex[pinput - 1] == 2) {
                                        pawnindex = 1;
                                    }
                                    if (blackindex[pinput - 1] == 1) {
                                        pawnindex = 2;
                                    }
                                    break;
                                case 2:
                                    if (blackindex[pinput - 1] == 2) {
                                        pawnindex = 3;
                                    }
                                    if (blackindex[pinput - 1] == 1) {
                                        pawnindex = 4;
                                    }
                                    break;
                                case 3:
                                    if (blackindex[pinput - 1] == 2) {
                                        pawnindex = 5;
                                    }
                                    if (blackindex[pinput - 1] == 1) {
                                        pawnindex = 6;
                                    }
                                    break;
                            }
                            blackindex[pinput - 1] = blackindex[pinput - 1] - 1;
                            game.setBlackindex(blackindex);
                            pawn[pawnindex][0] = row_input;
                            pawn[pawnindex][1] = col_input;
                            end = true;
                        } else {System.out.println("Невалидна позиция");}
                    } else {
                        System.out.println("Нямате повече членове от този клас");
                    }
                } else {System.out.println("Нямаме такъв воиник");}

            }
        }



        return(pawn);
    }

    public static int[][] movepawn(boolean turn, int[][] redpawn, int[][]blackpawn ,int[][] wall, int[][] baricade){
        Scanner obj = new Scanner(System.in);
        boolean brreak = false;
        int[][] rpawn = new int[6][2];
        int[][] temppawn = new int[6][2];
        int[] laypawn = new int [2];
        int cluss = 0;
        if(turn){
            temppawn = redpawn;
            rpawn = redpawn;
        }else{
            temppawn = blackpawn;
            rpawn = blackpawn;
        }
        for(int i = 0;i <= 6;i++){
            if(temppawn[i][0] > 0){
                switch(i){
                    case 1,2:
                        System.out.println(i + "-Рицар-%  на позиция " + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                    case 3,4:
                        System.out.println(i +"-Джудже-@" + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                    case 5,6:
                        System.out.println(i +"-Елф-&  на позиция " + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                }
            }
        }

        System.out.println("Изберете фигура");
        int temp = obj.nextInt();
            switch(temp){
                case 1,2:
                    cluss=1;
                    break;
                case 3,4:
                    cluss=2;
                    break;
                case 5,6:
                    cluss=3;
                    break;
            }

        switch(cluss){
            case 1:
                System.out.println("Рицар-% може да се движи само към съседните си 4 зони + \n" +
                        "изберете посока(w,a,s,d)");
                while(!brreak){
                    int pawndir;
                    int[] dir = new int[2];
                    switch(obj.next().charAt(0)){
                        case 'w':
                            pawndir = 0;
                            dir[0] = -1;
                            brreak = true;
                           break;
                        case 's':
                            pawndir = 0;
                            dir[0] = 1;
                            brreak = true;
                            break;
                        case 'a':
                            pawndir = 1;
                            dir[1] = -1;
                            brreak = true;
                            break;
                        case 'd':
                            pawndir = 1;
                            dir[1] = 1;
                            brreak = true;
                            break;
                        default:
                            pawndir = 0;
                            break;

                    }
                    laypawn[0] = temppawn[temp][0] + dir[0];
                    laypawn[1] = temppawn[temp][1] + dir[1];

                    if(collisioncheck(laypawn,redpawn,blackpawn,baricade,wall,pawndir)){
                        temppawn[temp][pawndir] = temppawn[temp][pawndir] + dir[0];
                        temppawn[temp][pawndir] = temppawn[temp][pawndir] + dir[1];
                        rpawn = temppawn;
                    } else {System.out.println("Нещо блокира пътя");}
                }
            break;
            case 2:
                System.out.println("Джудже-@ може да се движи само по прави на максимум 2 зони на веднъж + \n" +
                        "разтояние 1-2 и посока(w,a,s,d) ");
                while(!brreak){
                    int pawndir;
                    int[] dir = new int[2];
                    int lenght;
                    if(obj.nextInt() <= 1 ){lenght = 1;} else {lenght = 2;}
                    switch(obj.next().charAt(0)){
                        case 'w':
                            pawndir = 0;
                            dir[0] = -lenght;
                            brreak = true;
                            break;
                        case 's':
                            pawndir = 0;
                            dir[0] = lenght;
                            brreak = true;
                            break;
                        case 'a':
                            pawndir = 1;
                            dir[1] = -lenght;
                            brreak = true;
                            break;
                        case 'd':
                            pawndir = 1;
                            dir[1] = lenght;
                            brreak = true;
                            break;
                        default:
                            pawndir = 0;
                            break;

                    }
                    laypawn[0] = temppawn[temp][0] + dir[0];
                    laypawn[1] = temppawn[temp][1] + dir[1];

                    if(collisioncheck(laypawn,redpawn,blackpawn,baricade,wall,pawndir)){
                        temppawn[temp][pawndir] = temppawn[temp][pawndir] + dir[0];
                        temppawn[temp][pawndir] = temppawn[temp][pawndir] + dir[1];
                        rpawn = temppawn;
                    } else {System.out.println("Нещо блокира пътя");}
                }
                break;
            case 3:
                System.out.println("елф-& може да се движи до 3 зони на право и г образно  + \n" +
                        "разтояние 1-3 (със завои ако 3) и посока(w,a,s,d) и посока на завои l:r (ако искате)");
                while(!brreak){
                    int pawndir;
                    int[] dir = new int[2];
                    int lenght = obj.nextInt();
                    System.out.println("посока(w,a,s,d)");
                    switch(obj.next().charAt(0)){
                        case 'w':
                            pawndir = 0;
                            dir[0] = -lenght;
                            brreak = true;
                            break;
                        case 's':
                            pawndir = 0;
                            dir[0] = lenght;
                            brreak = true;
                            break;
                        case 'a':
                            pawndir = 1;
                            dir[1] = -lenght;
                            brreak = true;
                            break;
                        case 'd':
                            pawndir = 1;
                            dir[1] = lenght;
                            brreak = true;
                            break;
                        default:
                            pawndir = 0;
                            break;

                    }
                    int stemp;
                    if(lenght <= 2){
                        stemp = 0;
                    } else {
                        System.out.println("В коя посока ще завиете l:r");
                        switch(obj.next().charAt(0)){
                            case 'l':
                                stemp = -1;
                                break;
                            case 'r':
                                stemp = 1;
                                break;
                            default:
                                stemp = 0;
                                break;
                        }
                    }
                        laypawn[0] = temppawn[temp][0] + dir[0] + stemp;
                        laypawn[1] = temppawn[temp][1] + dir[1] + stemp;
                        if(collisioncheck(laypawn,redpawn,blackpawn,baricade,wall,pawndir)) {
                        temppawn[temp][0] = temppawn[temp][0] + dir[0] + stemp;
                        temppawn[temp][1] = temppawn[temp][1] + dir[1] + stemp;
                        rpawn = temppawn;
                    }   else {System.out.println("Нещо блокира пътя");}



                }
                break;



        }

        return(rpawn);
    }
    public static boolean collisioncheck(int[] object, int[][] redpawn,int[][] blackpawn, int[][] barricade,int[][] wall, int dir){
        boolean ret = false;
        for(int i = 0;i <=6;i++){
            if(!Arrays.equals(object, redpawn[i]) && !Arrays.equals(object, blackpawn[i]) && !Arrays.equals(object, barricade[i]) && !Arrays.equals(object, wall[i])){
                ret = true;
            } else {ret = false; break;}
        }
        return(ret);
    }
    public static int[][] possbileattacks(boolean turn, int[][] redpawn,int[][] blackpawn,int attacker){
        int[] attackerpawn = {0,0};
        int[][] team = new int[8][2];
        int[][] target = new int[8][2];
        int[][] targetlist = new int[8][2];
        int[] pawn;
        int cluss;
        for(int i = 1; i<=6;i++){
            targetlist[i][0] = 0;
            targetlist[i][0] = 1;
        }
        if(turn){team = redpawn;target = blackpawn;} else {team = blackpawn;target = redpawn;}
        if(attacker == 0) {
            for (int i = 1; i <= 6; i++) {
                switch (i) {
                    case 1, 2:
                        cluss = 1;
                        break;
                    case 3, 4:
                        cluss = 2;
                        break;
                    case 5, 6:
                        cluss = 3;
                        break;
                    default:
                        cluss = 0;
                }
                for(int y = 1;y <=6;y++){
                            if(target[y][0] - cluss == team[i][0] && target[y][1] == team[i][1]) {targetlist[y] = target[y];}
                            if(target[y][0] + cluss == team[i][0] && target[y][1] == team[i][1]) {targetlist[y] = target[y];}
                            if(target[y][0] == team[i][0] && target[y][1] - cluss == team[i][1]) {targetlist[y] = target[y];}
                            if(target[y][0] == team[i][0] && target[y][1] + cluss == team[i][1]) {targetlist[y] = target[y];}


                }
            }
        }

        return(targetlist);
    }
    public static int[] pawnselect(boolean turn,int[][] redpawn, int[][]blackpawn){
        Scanner obj = new Scanner(System.in);
        int cluss = 0;
        int[][]temppawn;
        if(turn){
            temppawn = redpawn;
        }else{
            temppawn = blackpawn;
        }
        for(int i = 0;i <= 6;i++){
            if(temppawn[i][0] > 0){
                switch(i){
                    case 1,2:
                        System.out.println(i + "-Рицар-%  на позиция " + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                    case 3,4:
                        System.out.println(i +"-Джудже-@ на позиция" + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                    case 5,6:
                        System.out.println(i +"-Елф-&  на позиция " + temppawn[i][0] + "-" + temppawn[i][1]);
                        break;
                }
            }
        }
        System.out.println("Изберете фигура");
        int temp = obj.nextInt();
        switch(temp){
            case 1,2:
                cluss=1;
                break;
            case 3,4:
                cluss=2;
                break;
            case 5,6:
                cluss=3;
                break;
        }
        int[] pawn = {temp,cluss};
        return(pawn);
    }
    public static int[][] pawnattack(boolean turn,int[][]redpawn, int[][]blackpawn,int attackerid ,int[][] targetlist,int[][] health,int[][] barricade){
        Scanner obj = new Scanner(System.in);
        int[][] avalable_target = new int[8][2];
        int[][] attackerlist = new int[8][2];
        int[] nullnull = {0,0};
        int cluss;
        int enemy_cluss = 0;
        int iturn;
        int targetid = 0;
        int damage = 0;
        String symbol = "";
        if(turn){
            attackerlist = redpawn;
            iturn = 0;
        }else{
            attackerlist = blackpawn;
            iturn = 1;
        }

        switch (attackerid) {
            case 1, 2:
                cluss = 1;
                damage = 8;
                break;
            case 3, 4:
                cluss = 2;
                damage = 5;
                break;
            case 5, 6:
                cluss = 3;
                damage = 6;
                break;
            default:
                cluss = 0;
        }

        for(int y = 1;y <=6;y++){
            if(targetlist[y][0] - cluss == attackerlist[attackerid][0] && targetlist[y][1] == attackerlist[attackerid][1]) {avalable_target[y] = targetlist[y]; targetid = y;}
            if(targetlist[y][0] + cluss == attackerlist[attackerid][0] && targetlist[y][1] == attackerlist[attackerid][1]) {avalable_target[y] = targetlist[y]; targetid = y;}
            if(targetlist[y][0] == attackerlist[attackerid][0] && targetlist[y][1] - cluss == attackerlist[attackerid][1]) {avalable_target[y] = targetlist[y]; targetid = y;}
            if(targetlist[y][0] == attackerlist[attackerid][0] && targetlist[y][1] + cluss == attackerlist[attackerid][1]) {avalable_target[y] = targetlist[y]; targetid = y;}
        }

        System.out.println("изберете кого да атакувате");
        for(int i = 1;i <=6;i++){
            switch (i) {
                case 1, 2:
                    enemy_cluss = 1;
                    symbol = "рицар-% на";
                    break;
                case 3, 4:
                    enemy_cluss = 2;
                    symbol = "джудже-& на";
                    break;
                case 5, 6:
                    enemy_cluss = 3;
                    symbol = "елф-@ на";
                    break;
            }
            if(Arrays.equals(avalable_target[i],nullnull)){
            } else
                {
                System.out.println("("+i+")-"+symbol + Arrays.toString(avalable_target[i]));
                }
            }

        int input = obj.nextInt();
        if(Arrays.equals(avalable_target[input],nullnull)){
            System.out.println("невалиден противник");
        } else {
            System.out.println("хвърляме 3 зара");
            int roll = ((int) ((Math.random() * (6 - 1))) + 1) + ((int) ((Math.random() * (6 - 1))) + 1) + ((int) ((Math.random() * (6 - 1))) + 1);
            if(roll == health[iturn][input])
            {
                System.out.println("Пропуснахме - 0dmg");
                damage = 0;
            } else if(roll == 3) {
                damage = damage / 2;
            }
            switch(enemy_cluss){
                case 1:
                    if(damage - 3 > 0) {
                        health[iturn][input] = health[iturn][input] - (damage - 3);
                        System.out.println("УДАР - " + (damage - 3) + "dmg : " + health[iturn][input] + "HP");

                    } else {
                        System.out.println("ударът беше блокиран - 0dmg");
                    }
                case 2:
                    if(damage - 1 > 0) {
                        health[iturn][input] = health[iturn][input] - (damage - 1);
                        System.out.println("УДАР - " + (damage - 1) + "dmg : " + health[iturn][input] + "HP");
                    } else {
                        System.out.println("ударът беше блокиран - 0dmg");
                    }
                case 3:
                    if(damage - 2 > 0) {
                        health[iturn][input] = health[iturn][input] - (damage - 2);
                        System.out.println("УДАР - " + (damage - 2) + "dmg : " + health[iturn][input] + "HP");
                    } else {
                        System.out.println("ударът беше блокиран - 0dmg");
                    }
            }
        }


        return(health);
    }
    public static int[][] pawnheal(boolean turn,int[][] heath,int[][] heathpotion,int id,int cluss){
        int iturn;
        if(turn){iturn = 0;}else{iturn = 1;}
        heath[iturn][id] = heath[iturn][id] + (int) ((Math.random() * (6 - 1))) + 1;
        switch(cluss){
            case 1:
                if(heath[iturn][id] > 15){heath[iturn][id] = 15;}
            case 2:
                if(heath[iturn][id] > 10){heath[iturn][id] = 10;}
            case 3:
                if(heath[iturn][id] > 12){heath[iturn][id] = 12;}
        }

        return(heath);
    }
}


